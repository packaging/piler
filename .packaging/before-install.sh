getent passwd piler >/dev/null 2>&1 || adduser \
    --system \
    --shell /bin/sh \
    --gecos 'piler' \
    --group \
    --disabled-password \
    --home /var/lib/piler \
    piler
if command -v systemctl >/dev/null 2>&1; then
    debsystemctl="$(command -v deb-systemd-invoke || echo systemctl)"
    $debsystemctl mask manticore.service
fi
