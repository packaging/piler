if command -v systemctl >/dev/null 2>&1; then
    debsystemctl="$(command -v deb-systemd-invoke || echo systemctl)"
    systemctl --system daemon-reload >/dev/null || true
    if ! systemctl is-enabled piler-searchd.service >/dev/null; then
        $debsystemctl restart piler-searchd.service
    fi
    if ! systemctl is-enabled piler.service >/dev/null; then
        $debsystemctl restart piler.service
    fi
    if ! systemctl is-enabled piler-smtp.service >/dev/null; then
        $debsystemctl restart piler-smtp.service
    fi
fi
