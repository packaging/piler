# [piler](http://mailpiler.org/) unofficial packages

**If you like what mail piler is doing for you and probably your company, please consider supporting Janos by subscribing to the [piler enterprise version](https://mailpiler.com/) which also adds features!**

This project just builds the open source piler version according to the [instructions](http://www.mailpiler.org/wiki/current:installation) and creates a package using [fpm](https://github.com/jordansissel/fpm/).

For detailed installation instructions and more info please see the [wiki](https://gitlab.com/packaging/piler/wikis/home).

## notable changes

* using systemd timers instead of cron
* using systemd for all services
