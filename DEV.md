# Dev

## Build locally

```bash
DOCKER_DEFAULT_PLATFORM=linux/amd64 docker run --rm -it --tmpfs /tmp:exec -w /tmp -v $PWD:/source -e XLHTML_VERSION="0.5.1" -e CI_PROJECT_DIR=/source -e CI_COMMIT_TAG=1.4.4+1 ubuntu:jammy
/source/.gitlab-ci/xlhtml.sh
/source/.gitlab-ci/piler.sh
```
