#!/bin/bash

set -eu
export CCACHE_DIR="${CCACHE_DIR:-${CI_PROJECT_DIR}/ccache}"
. /etc/lsb-release
apt-get update
apt-get -y install curl ruby ruby-dev make gcc git ccache
mkdir -p "${CCACHE_DIR}"
type fpm >/dev/null 2>&1 || (
    gem install dotenv -v 2.8.1 --no-doc
    gem install fpm --no-doc
)
curl -sLO "https://bitbucket.org/jsuto/piler/downloads/xlhtml-${XLHTML_VERSION}-sj-mod.tar.gz"
tar xzf "xlhtml-${XLHTML_VERSION}-sj-mod.tar.gz"
cd "xlhtml-${XLHTML_VERSION}-sj-mod"
./configure --prefix=/usr
make CC="ccache gcc"
tmpdir="$(mktemp -d)"
make install DESTDIR="${tmpdir}"
mkdir -p "${CI_PROJECT_DIR}/upload/${DISTRIB_CODENAME}"
fpm \
    -f \
    -s dir \
    -t deb \
    --log error \
    --name xlhtml-sj-mod \
    --package "${CI_PROJECT_DIR}/upload/${DISTRIB_CODENAME}/xlhtml-sj-mod_${XLHTML_VERSION}-${DISTRIB_CODENAME}_amd64.deb" \
    --version "${XLHTML_VERSION}-${DISTRIB_CODENAME}" \
    --description "Convert Excel/PowerPoint to html" \
    --url "http://www.mailpiler.org/" \
    --license GPLv3 \
    --maintainer "Stefan Heitmüller <stefan.heitmueller@gmx.com>" \
    --prefix / \
    -C "${tmpdir}" usr

# for local docker builds
if [ -d "${CI_PROJECT_DIR}/upload/${DISTRIB_CODENAME}" ]; then
    if [ ! "${PWD}" == "${CI_PROJECT_DIR}" ]; then
        owner="$(stat -c %u "${CI_PROJECT_DIR}")"
        group="$(stat -c %g "${CI_PROJECT_DIR}")"
        chown -R "${owner}":"${group}" "${CI_PROJECT_DIR}/upload"
    fi
fi
