#!/bin/bash

set -eu
export CCACHE_DIR="${CCACHE_DIR:-${CI_PROJECT_DIR}/ccache}"
. /etc/lsb-release

tag="${CI_COMMIT_TAG:-1.4.4+1}"
version="${tag%%+*}"
patchlevel="${tag##*+}"

python_mysqsldb="python3-mysqldb"
libzip="libzip4"

case "${DISTRIB_CODENAME}" in
"bionic")
    python_mysqsldb="python-mysqldb"
    ;;
"focal")
    libzip="libzip5"
    ;;
*)
    :
    ;;
esac

depends="${python_mysqsldb} ${libzip}"
buildpackages="${depends} libzip-dev"

printf "\e[1;36minstalling toolchain ...\e[0m\n"
printf "\e[0;36m  base\e[0m\n"
apt-get update
apt-get -y install \
    ccache \
    git \
    curl \
    ruby \
    ruby-dev \
    make \
    gcc \
    poppler-utils \
    libpst4 \
    libtre5 \
    sysstat \
    catdoc \
    unrtf \
    tnef \
    libwrap0-dev \
    libssl-dev \
    libtre-dev \
    zlib1g-dev \
    libmysqlclient-dev \
    $buildpackages
if ! dpkg -s xlhtml-sj-mod >/dev/null 2>&1; then
    printf "\e[0;36m  xlhtml\e[0m\n"
    dpkg -i "${CI_PROJECT_DIR}/upload/${DISTRIB_CODENAME}/"xlhtml-sj-mod_*_amd64.deb
fi
type fpm >/dev/null 2>&1 || (
    printf "\e[0;36m  fpm\e[0m\n"
    gem install dotenv -v 2.8.1 --no-doc
    gem install fpm --no-doc
)
getent passwd piler >/dev/null 2>&1 || adduser \
    --system \
    --shell /bin/sh \
    --gecos 'piler' \
    --group \
    --disabled-password \
    --home /var/lib/piler \
    piler

if [ ! -d "piler-${version}" ]; then
    if ! git clone --depth 1 -b "piler-${version}" https://bitbucket.org/jsuto/piler.git "piler-${version}"; then
        if [ ! -f "piler-${version}.tar.gz" ]; then
            printf "\e[0;36m  fetching %s\e[0m\n" "piler-${version}"
            curl -sLO "https://bitbucket.org/jsuto/piler/downloads/piler-${version}.tar.gz"
        fi
        printf "\e[0;36m  extracting %s\e[0m\n" "piler-${version}"
        tar xzf "piler-${version}.tar.gz"
    fi
fi
cd "piler-${version}"

printf "\e[0;36m  patching /run/piler paths\e[0m\n"
sed -i 's,$(localstatedir)/run/piler,/run/piler,' etc/Makefile.in

printf "\e[1;36mcompiling source\e[0m\n"
# (com)piler :)
localstatedir="/var/lib"
./configure \
    --prefix=/ \
    --exec_prefix=/usr \
    --datarootdir=/usr/share \
    --localstatedir="${localstatedir}" \
    --with-database=mysql \
    --enable-tcpwrappers \
    --enable-memcached \
    --enable-clamd
make CC="ccache gcc"

printf "\e[1;36mpreparing package\e[0m\n"
tmpdir="$(mktemp -d)"
make install DESTDIR="${tmpdir}"

printf "\e[0;36m  patching /var/lib/piler paths\e[0m\n"
rgrep -l "var/piler" "${tmpdir}" | xargs sed -i 's,/var/piler,/var/lib/piler,g'

printf "\e[0;36m  patching webui reload command\e[0m\n"
sed -i 's,sudo -n /etc/init.d/rc.piler reload,sudo -n /bin/systemctl reload piler,g' "${tmpdir}${localstatedir}/piler/www/config.php"
find "${tmpdir}${localstatedir}/piler/www" -type f -name messages.php | xargs sed -i 's,/etc/init.d/rc.piler reload,/bin/systemctl reload piler,'


printf "\e[0;36m  adding https by default\e[0m\n"
cp -rv "util" "${tmpdir}${localstatedir}/piler/"
sed -i "s,['SITE_URL'] = 'http://',['SITE_URL'] = 'https://'," "${tmpdir}${localstatedir}/piler/util/config-site.php.in"

printf "\e[0;36m  adding realtime index\e[0m\n"
cp -rv "${CI_PROJECT_DIR}/.packaging/${localstatedir}/piler/util" "${tmpdir}${localstatedir}/piler/util"
if ! grep -q "SPHINX_MAIN_INDEX.*piler1" "${tmpdir}${localstatedir}/piler/util/config-site.php.in"; then
    echo "" | tee -a "${tmpdir}${localstatedir}/piler/util/config-site.php.in"
    echo "config['SPHINX_MAIN_INDEX'] = 'piler1';" | tee -a "${tmpdir}${localstatedir}/piler/util/config-site.php.in"
    echo "" | tee -a "${tmpdir}${localstatedir}/piler/util/config-site.php.in"
fi
sed -i "s%define('RT', 0);%define('RT', 1);%" "${tmpdir}/etc/piler/manticore.conf.dist"

printf "\e[0;36m  adding systemd units\e[0m\n"
cp -rv "${CI_PROJECT_DIR}/.packaging/usr" "${tmpdir}/"
cp -rv "${CI_PROJECT_DIR}/.packaging/var" "${tmpdir}/"
cp -rv "${CI_PROJECT_DIR}/.packaging/etc" "${tmpdir}"/
printf "\e[0;36m  enabling manticore\e[0m\n"
touch "${tmpdir}/etc/piler/MANTICORE"

printf "\e[0;36m  remove legacy init.d files\e[0m\n"
rm -rf "${tmpdir}/etc/init.d"

printf "\e[1;36mcreating package\e[0m\n"
mkdir -p "${CI_PROJECT_DIR}/upload/${DISTRIB_CODENAME}"
fpm \
    -f \
    -s dir \
    -t deb \
    --log error \
    --name piler-ce \
    --package "${CI_PROJECT_DIR}/upload/${DISTRIB_CODENAME}/piler-ce_${version}+${patchlevel}-${DISTRIB_CODENAME}_amd64.deb" \
    --description "Piler is a feature rich open source email archiving solution" \
    --url "http://www.mailpiler.org/" \
    --version "${version}+${patchlevel}" \
    --license GPLv3 \
    --maintainer "Stefan Heitmüller <stefan.heitmueller@gmx.com>" \
    --prefix / \
    --deb-dist "${DISTRIB_CODENAME}" \
    --deb-user piler \
    --deb-group piler \
    --depends libcap2-bin \
    --depends bash \
    --depends tnef \
    --depends unrtf \
    --depends catdoc \
    --depends manticore \
    --depends manticore-columnar-lib \
    --depends poppler-utils \
    --depends libpst4 \
    --depends libtre5 \
    --depends sysstat \
    --depends memcached \
    --depends xlhtml-sj-mod \
    --depends php-bz2 \
    --depends php-cli \
    --depends php-common \
    --depends php-curl \
    --depends php-gd \
    --depends php-json \
    --depends php-ldap \
    --depends php-mbstring \
    --depends php-mysql \
    --depends php-opcache \
    --depends php-readline \
    --depends php-xml \
    --depends php-zip \
    --depends php-apcu \
    --depends mariadb-server \
    --depends systemd \
    --depends gettext-base \
    --depends ${depends// / --depends } \
    --before-install "${CI_PROJECT_DIR}/.packaging/before-install.sh" \
    --after-install "${CI_PROJECT_DIR}/.packaging/after-install.sh" \
    --after-upgrade "${CI_PROJECT_DIR}/.packaging/after-upgrade.sh" \
    -C "${tmpdir}" usr etc var

# for local docker builds
if [ -d "$CI_PROJECT_DIR/upload/$DISTRIB_CODENAME" ]; then
    if [ ! "${PWD}" == "${CI_PROJECT_DIR}" ]; then
        owner="$(stat -c %u "${CI_PROJECT_DIR}")"
        group="$(stat -c %g "${CI_PROJECT_DIR}")"
        chown -R "${owner}":"${group}" "${CI_PROJECT_DIR}/upload"
    fi
fi
